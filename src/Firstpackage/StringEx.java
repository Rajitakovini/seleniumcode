package Firstpackage;

public class StringEx {
public static void main(String[]args) {
	String S3 = "Raj"; // creating string  by using string literal
	
	char c [] = {'r','a','j','i','t','a'};
	String objS= new String(c); // converting char array into string
	
	String objS1 = new String("welcome"); // creating java string using new keyword
	
	System.out.println(objS);
	System.out.println(objS1);
	System.out.println(S3);
}
}
